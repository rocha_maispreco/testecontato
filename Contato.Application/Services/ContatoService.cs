﻿using AutoMapper;
using Contatos.Service.ViewModel;
using Contatos.Infraestrutura.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contatos.Domain.Entities;

namespace Contatos.Service.Services
{
    public class ContatoService
    {
        private readonly IContatoRepository _contatoRepository;
        private readonly IMapper _mapper;

        public ContatoService(IMapper mapper, IContatoRepository contatoRepository)
        {
            _contatoRepository = contatoRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ContatoViewModel>> GetAll()
        {

            var listaContato = _contatoRepository.AsQueryable().Where(x => x.FlAtivo).ToList();
            if (listaContato.Count == 0)
                return null;
            var listaContatoViewModel = _mapper.Map<IEnumerable<ContatoViewModel>>(listaContato);

            foreach (var item in listaContatoViewModel)
            {
                item.Idade = CalculaIdade(listaContato.Where(x => x.Id == item.Id).Select(y => y.DataNascimento).FirstOrDefault());
            }
            return listaContatoViewModel;
        }

        public async Task<ContatoViewModel> GetById(int id)
        {
            var retorno = _contatoRepository.AsQueryable().Where(p => p.Id == id && p.FlAtivo).FirstOrDefault();
            if (retorno == null)
                return null;

            var contato = _mapper.Map<ContatoViewModel>(retorno);

            contato.Idade = CalculaIdade(retorno.DataNascimento);

            return contato;
        }

       
        public async Task<ContatoViewModel> Incluir(ContatoViewModel obj)
        {
            var objRetorno = new ContatoViewModel();

            try
            {
                var contato =  _mapper.Map<Contato>(obj);
                if(CalculaIdade(obj.DataNascimento) < 18)
                        return null;
               var retorno = await _contatoRepository.Insert(contato);
                objRetorno = _mapper.Map<ContatoViewModel>(retorno);
            }
            catch(Exception ex)
            {
                return null;
            }

            return objRetorno;
        }

        public async Task<ErrorMessage> Excluir(int id)
        {
            var objRetorno = new ErrorMessage();

            try
            {
                
                var contato = _contatoRepository.AsQueryable().Where(p => p.Id == id).FirstOrDefault();
                if(contato == null)
                {
                    objRetorno.Valido = false;
                    objRetorno.Erro = "Objeto não existe no sistema.";
                    return objRetorno;
                }
                _contatoRepository.Delete(contato);
                objRetorno.Valido =true;
            }
            catch (Exception ex)
            {
                objRetorno.Valido = false;
                objRetorno.Erro = "Erro ao excluir dados! " + ex.Message;
            }

            return objRetorno;
        }

        public async Task<ContatoViewModel> Alterar(ContatoViewModel obj)
        {
            var objRetorno = new ContatoViewModel();

            try
            {
                if (CalculaIdade(obj.DataNascimento) < 18)
                    return null;
                var contato = _mapper.Map<Contato>(obj);

               await _contatoRepository.Update(contato);
                objRetorno = obj;
            }
            catch (Exception ex)
            {
                return null;
            }

            return objRetorno;
        }

        private int CalculaIdade(DateTime dataNascimento)
        {
            int idade = DateTime.Now.Year - dataNascimento.Year;
            if (DateTime.Now.DayOfYear < dataNascimento.DayOfYear)
            {
                idade = idade - 1;
            }
            return idade;
        }
    }
}

﻿using Contatos.Service.Services;
using Contatos.Service.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Contatos.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ContatoController : ControllerBase
    {
        private readonly ContatoService _contaService;

        public ContatoController(ContatoService contatoService)
        {
            _contaService = contatoService;
        }

        [HttpGet]
        public async Task<IEnumerable<ContatoViewModel>> Get()
        {
            return await _contaService.GetAll();
           
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _contaService.Excluir(id);
            if (result.Valido)
                return Ok(result);
            else
                return BadRequest("Erro ao excluir registro. " + result.Erro);
        }

        [HttpPost]
        public ActionResult<ContatoViewModel> Create([FromForm] ContatoViewModel contato)
        {
            var model = _contaService.Incluir(contato).Result;

            if (model != null)
                return Ok(model);
            else
                return BadRequest("Erro ao inserir registro. ");

        }

        [HttpPut]
        public ActionResult Update([FromForm] ContatoViewModel contato)
        {
            var model = _contaService.Alterar(contato).Result;

            if (model != null)
                return Ok(model);
            else
                return BadRequest("Erro ao alterar registro. ");

        }

        [HttpGet("contato/{id}")]
        public async Task<IActionResult> GetById(int id)
        {

            var result = await _contaService.GetById(id);
            if (result != null)
                return Ok(result);
            else
                return NotFound("Registro não encontrado");
        }
    }
}

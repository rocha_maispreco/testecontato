﻿using Contatos.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Teste.Infrastructure.Data.Config
{
    public class ContatoConfig : IEntityTypeConfiguration<Contato>
    {
        public void Configure(EntityTypeBuilder<Contato> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.NomeContato).IsRequired().HasMaxLength(100);
            builder.Property(p => p.DataNascimento).HasColumnType("datetime");
            builder.Property(p => p.Sexo);
            builder.Property(p => p.FlAtivo);
            builder.Property(e => e.ModifiedDate).HasColumnType("datetime");
            builder.Property(e => e.CreatedDate).HasColumnType("datetime");
        }
    }
}

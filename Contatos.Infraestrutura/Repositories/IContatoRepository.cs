﻿using Contatos.Domain.Entities;

namespace Contatos.Infraestrutura.Data.Repositories
{
    public interface IContatoRepository : IRepository<Contato>
    {
    }
}

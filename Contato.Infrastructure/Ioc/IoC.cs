﻿using Contatos.Infrastructure.Data.Context;
using Contatos.Infrastructure.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Contatos.Infrastructure.Ioc
{
    public static class IoC
    {

        private const string StringConnection = "CONNECTION_SQL";
        public static IServiceCollection AddDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ContatoContext>(options =>
                        options.UseLazyLoadingProxies().UseSqlServer(configuration.GetValue<string>(StringConnection)));

            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services) =>
            services.AddScoped<IContatoRepository, ContatoRepository>();
    }
}

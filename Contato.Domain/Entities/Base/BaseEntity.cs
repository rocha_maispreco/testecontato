﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Contatos.Domain.Entities.Base
{
   public abstract class BaseEntity
    {
        [NotMapped]
        public int Id { get; set; }
        [NotMapped]
        public DateTime? CreatedDate { get; set; }
        [NotMapped]
        public DateTime? ModifiedDate { get; set; }
    }
}

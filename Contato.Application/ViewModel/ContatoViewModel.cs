﻿using Contatos.Domain.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Contatos.Service.ViewModel
{
    public class ContatoViewModel 
    {
        public int Id { get; set; }
        [Display(Name = "Nome Contato")]
        public string NomeContato { get; set; }
        public DateTime DataNascimento { get; set; }
        public bool FlAtivo { get; set; }
        public string Sexo { get; set; }
        [NotMapped]
        public int Idade { get; set; }
    }
}

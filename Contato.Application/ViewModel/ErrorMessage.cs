﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contatos.Service.ViewModel
{
    public class ErrorMessage
    {
        public bool Valido { get; set; }
        public string Erro { get; set; }
    }
}

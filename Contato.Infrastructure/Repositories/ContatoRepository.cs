﻿using Contatos.Domain.Entities;
using Contatos.Infrastructure.Data.Context;

namespace Contatos.Infrastructure.Data.Repositories
{
    public class ContatoRepository : Repository<Contato>, IContatoRepository
    {
        public ContatoRepository(ContatoContext context) : base(context) { }
    }
}

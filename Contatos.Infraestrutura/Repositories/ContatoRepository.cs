﻿using Contatos.Domain.Entities;
using Contatos.Infraestrutura.Data.Context;

namespace Contatos.Infraestrutura.Data.Repositories
{
    public class ContatoRepository : Repository<Contato>, IContatoRepository
    {
        public ContatoRepository(ContatoContext context) : base(context) { }
    }
}

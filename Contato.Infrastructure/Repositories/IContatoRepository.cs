﻿using Contatos.Domain.Entities;

namespace Contatos.Infrastructure.Data.Repositories
{
    public interface IContatoRepository : IRepository<Contato>
    {
    }
}

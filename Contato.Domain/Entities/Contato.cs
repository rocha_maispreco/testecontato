﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Contatos.Domain.Entities.Base;

namespace Contatos.Domain.Entities
{
    public class Contato : BaseEntity
    {
        [Display(Name = "Nome Contato")]
        public string NomeContato { get; set; }
        public DateTime DataNascimento { get; set; }
        public bool FlAtivo { get; set; }
        public string Sexo { get; set; }
    }
}

﻿using Contatos.Infraestrutura.Data.Context;
using Contatos.Infraestrutura.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Contatos.Infraestrutura.Ioc
{
    public static class IoC
    {
        private const string ConnectionStringKey = "CONNECTION_SQL";
        public static IServiceCollection AddDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ContatoContext>(options =>
                        options.UseLazyLoadingProxies().UseSqlServer(configuration.GetValue<string>(ConnectionStringKey)));

            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services) =>
            services.AddScoped<IContatoRepository, ContatoRepository>();
    }
}

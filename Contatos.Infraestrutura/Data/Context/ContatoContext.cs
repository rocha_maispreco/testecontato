using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Contatos.Domain.Entities;
using Contatos.Domain.Entities.Base;

namespace Contatos.Infraestrutura.Data.Context
{
    public class ContatoContext : DbContext
    {
        public ContatoContext(DbContextOptions options) : base(options) { }

        public DbSet<Contato> Contato { get; set; }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = new CancellationToken())
        {
            AddCreatedDate();
            AddModifiedDate();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void AddCreatedDate() =>
            ChangeTracker.Entries().Where(p => p.Entity is BaseEntity && p.State.Equals(EntityState.Added)).ToList()
                .ForEach(p => ((BaseEntity)p.Entity).CreatedDate = DateTime.UtcNow);

        private void AddModifiedDate() =>
            ChangeTracker.Entries().Where(p => p.Entity is BaseEntity && p.State.Equals(EntityState.Modified)).ToList()
                .ForEach(p => ((BaseEntity)p.Entity).ModifiedDate = DateTime.UtcNow);

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(modelBuilder);
        }
    }
}